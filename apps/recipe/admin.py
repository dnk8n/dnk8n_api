from django.contrib import admin
from apps.recipe.models import Category, Ingredient

admin.site.register(Category)
admin.site.register(Ingredient)
